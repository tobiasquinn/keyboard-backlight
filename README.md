# keyboard-backlight
Gnome Shell Keyboard Backlight Fader Extension

This uses the idle monitor and the primary monitor fullscreen state to fade out the keyboard backlight using the UPower interface when the configured idle timeout is reached.

It fades back in to the previous keyboard backlight value when the user is active or the primary monitor returns from fullscreen.

There is an archlinux PKGBUILD included.

(c) 2022, Tobias Quinn <tobias@tobiasquinn.com> License: GPLv3 
