all: prefs.ui ./schemas/gschema.compiled

eslint:
	./node_modules/.bin/eslint .

prefs.ui: prefs.glade.ui
	gtk4-builder-tool simplify --3to4 prefs.glade.ui > prefs.ui
	# change our PrefsWidget GtkGrid to be the template
	xmlstarlet ed -L -r "/interface/object[@id='PrefsWidget']" -v "template" prefs.ui
	# change class="GtkGrid" to parent="GtkGrid"
	xmlstarlet ed -L -r "/interface/template[@id='PrefsWidget']/@class" -v "parent" prefs.ui
	# change id="PrefsWidget" to class="PrefsWidget"
	xmlstarlet ed -L -r "/interface/template[@id='PrefsWidget']/@id" -v "class" prefs.ui

./schemas/gschema.compiled: ./schemas/com.tobiasquinn.keyboard-backlight.gschema.xml
	glib-compile-schemas ./schemas/
