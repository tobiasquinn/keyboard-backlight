/* -*- mode: js2; js2-basic-offset: 4; indent-tabs-mode: nil -*- */

const { GObject, Gtk, GLib } = imports.gi;
const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();

// NOTE: this allows this to be run using show_prefs.js for dev
const templateFilename = () => {
    try {
        return Me.dir.get_child('prefs.ui').get_uri();
    } catch (err) {
        return "file://" + GLib.get_current_dir() + "/prefs.ui";
    }
};

const PrefsWidget = GObject.registerClass({
    GTypeName: 'PrefsWidget',
    Template: templateFilename(),
    Children: ['idle_interval_spinbutton', 'fade_on_fullscreen_switch']
}, class PrefsWidget extends Gtk.Grid {
    _init(params = {}) {
        super._init(params);
        this._settings = ExtensionUtils.getSettings();
        this.idle_interval_spinbutton.set_value(this._settings.get_int('idle-timeout'));
        this.fade_on_fullscreen_switch.active = this._settings.get_boolean('fade-on-fullscreen');
    }

    idle_interval_spinbutton_value_changed_cb(spinbutton) {
        this._settings.set_int('idle-timeout', spinbutton.get_value());
    }

    fade_on_fullscreen_switch_state_set_cb(switch_) {
        this._settings.set_boolean('fade-on-fullscreen', switch_.active);
    }
});

var buildPrefsWidget = () => {
    return new PrefsWidget();
};

function init() {}
