/* -*- mode: js2; js2-basic-offset: 4; indent-tabs-mode: nil -*- */

/*
 * extension.js
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* exported init */
const Meta = imports.gi.Meta;
const Gio = imports.gi.Gio;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();

class Backlight {
    constructor() {
        const proxy = Gio.DBusProxy.makeProxyWrapper(
            `<node>
                <interface name="org.freedesktop.UPower.KbdBacklight">
                    <method name="GetBrightness">
                        <arg name="value" direction="out" type="i"/>
                    </method>
                    <method name="SetBrightness">
                        <arg name="value" direction="in" type="i"/>
                    </method>
                    <signal name="BrightnessChanged">
                        <arg name="value" direction="out" type="i"/>
                    </signal>
                    <signal name="BrightnessChangedWithSource">
                        <arg name="value" direction="out" type="i"/>
                        <arg name="source" direction="out" type="s"/>
                    </signal>
                </interface>
            </node>`
        );

        this.UPowerProxy = new proxy(
            Gio.DBus.system,
            'org.freedesktop.UPower',
            '/org/freedesktop/UPower/KbdBacklight'
        );

        this._brightness_handler_id = this.UPowerProxy.connectSignal("BrightnessChangedWithSource", (proxy, id, args) => this._brightness_changed(this, proxy, id, args));
        this._targetBrightness = this.UPowerProxy.GetBrightnessSync();
    }

    destroy() {
        this.UPowerProxy.disconnectSignal(this._brightness_handler_id);
    }

    fade_on() {
        //log("fade on to " + this._targetBrightness);
        this.UPowerProxy.SetBrightnessSync(this._targetBrightness);
    }

    fade_off() {
        //log("fade off");
        this.UPowerProxy.SetBrightnessSync(0);
    }

    _brightness_changed(_this, __proxy, __id, args) {
        const brightness = args[0];
        const source = args[1];
        log(`brightness changed ${__proxy} ${__id} ${brightness} ${source}`);
        // source is 'internal' for user commanded, 'external' otherwise
        if (args[1] == 'internal') {
            log(`storing new brightness ${brightness}`);
            _this._targetBrightness = brightness;
        }
    }
}

class Extension {
    enable() {
        this.backlight = new Backlight();
        this.settings = ExtensionUtils.getSettings();
        this._idle_monitor = Meta.get_backend().get_core_idle_monitor();

        this._we_are_fullscreen = false;
        this._we_are_idle = false;

        this._startIdler(this._get_timeout());

        // preferences change
        this._idle_timeout_changed = this.settings.connect('changed::idle-timeout', () => {
            this._restartIdler(this._get_timeout());
        });

        // main monitor is fullscreen
        this._in_fullscreen_changed = global.display.connect('in-fullscreen-changed', () => {
            this._we_are_fullscreen = this._is_fullscreen();
            this._setup_backlight();
        });
    }

    _get_timeout() {
        return this.settings.get_int('idle-timeout') * 1000;
    }

    _setup_backlight() {
        log(`keyboard-backlight ${new Date()} _we_are_fullscreen=${this._we_are_fullscreen} _settings=${this.settings.get_boolean('fade-on-fullscreen')} _we_are_idle=${this._we_are_idle}`);
        if ((this._we_are_fullscreen && this.settings.get_boolean('fade-on-fullscreen')) || this._we_are_idle)
            this.backlight.fade_off();
        else
            this.backlight.fade_on();
    }

    _startIdler(timeout) {
        this._idle_id = this._idle_monitor.add_idle_watch(timeout, () => {
            this._we_are_idle = true;
            this._setup_backlight();

            this._active_id = this._idle_monitor.add_user_active_watch(() => {
                this._we_are_idle = false;
                this._setup_backlight();
            });
        });
    }

    _restartIdler(timeout) {
        log(`restart with timeout ${timeout} seconds`);
        this._idle_monitor.remove_watch(this._idle_id);
        this._idle_monitor.remove_watch(this._active_id);
        this._startIdler(timeout);
    }

    disable() {
        if (this._idle_timeout_changed)
            this.settings.disconnect(this._idle_timeout_changed);
        if (this._in_fullscreen_changed)
            global.display.disconnect(this._in_fullscreen_changed);
        if (this.brightness)
            this.brightness.destroy();
        if (this._idle_id)
            this._idle_monitor.remove_watch(this._idle_id);
        if (this._active_id)
            this._idle_monitor.remove_watch(this._active_id);
    }

    _is_fullscreen() {
        return global.display.get_monitor_in_fullscreen(global.display.get_primary_monitor());
    }
}

function init() {
    log(`initialising ${Me.metadata.name}`);

    return new Extension();
}
