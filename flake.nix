{
  description = "A Gnome shell extension to manage the keyboard backlight fade on Thinkpads";

  outputs = { self, nixpkgs, ... }@inputs: {

    packages.x86_64-linux.keyboard-backlight =
      let pkgs = import nixpkgs {
            system = "x86_64-linux";
          };
      in pkgs.stdenv.mkDerivation {
        pname = "keyboard-backlight";
        version = "1.0.0";

        src = ./.;

        nativeBuildInputs = with pkgs; [ xmlstarlet gtk4.dev glib ];

        installPhase = ''
          export DIR=$out/share/gnome-shell/extensions/$pname@tobiasquinn.com
          mkdir -p $DIR/schemas
          cp metadata.json extension.js prefs.js prefs.ui $DIR
          cp schemas/gschemas.compiled $DIR/schemas
        '';
      };

    packages.x86_64-linux.default = self.packages.x86_64-linux.keyboard-backlight;

  };
}
